## Make Web Pages Dark
This extension is for FireFox. It will turn all you web pages into dark mode.
You can also change colors from the add-on preferences.

## TODO
* Create an icon to turn on/off for certain pages.
* Color picker in preferences.
 