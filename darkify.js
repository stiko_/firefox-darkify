let all = document.all;
let backgroundColor = "#313134";
let color = "#a2a2a4";

let optionsSet = 0;

function darkify() {
    optionsSet = 0;
    console.log("The color is: ", color);
    console.log("The background color is: ", backgroundColor);
    for (let i = 0; i < all.length; i++) {
        let elementColor = all[i].style.color;
        let elementBackgroundColor = all[i].style.backgroundColor;
        if (all[i].className.length > 0) {
            all[i].style.backgroundColor = backgroundColor;
            all[i].style.color = color;
        }

    }
}

function onGotColor(itemColor) {
    if (itemColor.color) {
        color = itemColor.color;

    }
    optionsSet++;
    if (optionsSet == 2) {
        darkify();

    }
}

function onGotBackgroundColor(itemBackgroundColor) {
    if (itemBackgroundColor.backgroundColor) {
        backgroundColor = itemBackgroundColor.backgroundColor;

    }
    optionsSet++;
    if (optionsSet == 2) {
        darkify()

    }
}

function onError(error) {
    console.log(`Error: ${error}`);
}

let storedColor = browser.storage.sync.get("color");
let storedBackgroundColor = browser.storage.sync.get("backgroundColor");

storedColor.then(onGotColor, onError);
storedBackgroundColor.then(onGotBackgroundColor, onError);


// darkify();
// document.body.style.backgroundColor = '#313134';
// document.body.style.color = '#a2a2a4';