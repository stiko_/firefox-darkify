function saveOptions(e) {
    e.preventDefault();
    let toSaveColor = document.querySelector("#color").value;
    let toSaveBackgroundColor = document.querySelector("#backgroundColor").value;
    console.log("Saving color is: ", toSaveColor);
    console.log("Saving background color is: ", toSaveBackgroundColor);
    browser.storage.sync.set({
        color: toSaveColor,
        backgroundColor: toSaveBackgroundColor
    });
}


function resetOptions(e) {
    e.preventDefault();
    console.log("Resetting colors");
    browser.storage.sync.set({
        color: "#a2a2a4",
        backgroundColor: "#313134"
    });
    restoreOptions();
}

function restoreColor() {
    function setCurrentChoice(result) {
        console.log("Retrieved color from storage: ");
        console.log(result.color);
        document.querySelector("#color").value = result.color || "#a2a2a4";
    }

    function onError(error) {
        console.log(`Could not set new colors Error: ${error}`);
    }

    let c = browser.storage.sync.get("color");
    c.then(setCurrentChoice, onError);
}

function restoreBackgroundColor() {
    function setCurrentChoice(result) {
        console.log("Retrieved background color from storage: ");
        console.log(result.backgroundColor);
        document.querySelector("#backgroundColor").value = result.backgroundColor || "#313134";
    }

    function onError(error) {
        console.log(`Could not set new colors Error: ${error}`);
    }

    let bc = browser.storage.sync.get("backgroundColor");
    bc.then(setCurrentChoice, onError);
}

function restoreOptions() {
    restoreColor();
    restoreBackgroundColor();

}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
document.querySelector("form").addEventListener("reset", resetOptions);